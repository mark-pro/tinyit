﻿using System;
using System.Threading.Tasks;
using Tiny.Data.Contexts;
using Tiny.Data.Models;

namespace Tiny.Data {
    public class TinySqlService : ITinyService, IDisposable, IAsyncDisposable {

        private readonly TinyDbContext _dbContext;

        public TinySqlService(TinyDbContext dbContext) =>
            (_dbContext) = (dbContext);


        public async Task<TinyUrl> InsertTinyUrlAsync(TinyUrl tinyUrl) {
            var result = await _dbContext.AddAsync(tinyUrl);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public bool TryGetLongUrl(string id , out string longUrl) =>
            (longUrl = _dbContext.Set<TinyUrl>().FindAsync(id).Result?.LongUrl) is not null;
        
        public void Dispose() => 
            _dbContext.Dispose();
        
        public ValueTask DisposeAsync() =>
            _dbContext.DisposeAsync();
        
        public async Task<bool> TinyUrlExists(string id) =>
            (await _dbContext.Set<TinyUrl>().FindAsync(id)) != null;
    }
}
