﻿using Raven.Client.Documents.Session;
using System;
using System.Threading.Tasks;
using Tiny.Data.Contexts;
using Tiny.Data.Models;

namespace Tiny.Data {
    public class TinyRavenService : ITinyService, IDisposable {

        private readonly IAsyncDocumentSession _session;

        public TinyRavenService(RavenDbContext dbContext) =>
            (_session) = (dbContext?.AsyncSession);

        public void Dispose() =>
            _session.Dispose();

        public async Task<TinyUrl> InsertTinyUrlAsync(TinyUrl tinyUrl) {
            await _session?.StoreAsync(tinyUrl , tinyUrl.ID);
            await _session?.SaveChangesAsync();
            return tinyUrl;
        }

        public bool TryGetLongUrl(string id , out string longUrl) =>
            (longUrl = (_session?.LoadAsync<TinyUrl>(id))?.Result?.LongUrl) is not null;
        
        public async Task<bool> TinyUrlExists(string id) =>
            await _session.Advanced.ExistsAsync(id);
    }
}
