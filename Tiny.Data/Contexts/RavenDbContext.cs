﻿using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Session;
using Raven.Client.Exceptions;
using Raven.Client.Exceptions.Database;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;
using Tiny.Data.Configuration;

namespace Tiny.Data.Contexts {
    public class RavenDbContext {

        private readonly RavenConfig _config;
        private readonly ILogger<RavenDbContext> _logger;

        public RavenDbContext(IOptions<RavenConfig> options , ILogger<RavenDbContext> logger) =>
            (_config , _logger) = (options.Value , logger);

        public RavenDbContext(Action<RavenConfig> action) =>
            action((_config = new RavenConfig()));

        public RavenDbContext(RavenConfig config) =>
            _config = config;

        private Lazy<IDocumentStore> LazyStore =>
            new Lazy<IDocumentStore>(() => 
                new DocumentStore {
                    Database = _config.DefaultDatabase ,
                    Urls = _config.Urls
                }.Initialize());

        public IDocumentStore Store => 
            LazyStore.Value;
        public IDocumentSession Session => 
            Store.OpenSession();
        public IAsyncDocumentSession AsyncSession => 
            Store.OpenAsyncSession();

        /// <summary>
        /// Attempts to create the databse. See
        /// <see href="https://ravendb.net/docs/article-page/4.2/csharp/client-api/operations/server-wide/create-database" />
        /// </summary>
        /// <returns>Returns whether or not the database exists</returns>
        public bool EnsureCreated() {
            try {
                Store.Maintenance.ForDatabase(_config.DefaultDatabase).Send(new GetStatisticsOperation());
                return true;
            }
            catch (DatabaseDoesNotExistException) {
                try {
                    Store.Maintenance.Server.Send(
                        new CreateDatabaseOperation(new DatabaseRecord {
                            DatabaseName = _config.DefaultDatabase
                        }));
                    _logger?.LogInformation("Sucessfully created database {database}" , _config.DefaultDatabase);
                    return true;
                }
                catch (ConcurrencyException) {
                    return true;
                }
                catch (RavenException rex) {
                    _logger?.LogError(rex , "Could not create database {database}" , _config.DefaultDatabase);
                    return false;
                }
            }
        }
    }
}
