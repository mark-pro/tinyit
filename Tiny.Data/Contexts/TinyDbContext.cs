﻿using Microsoft.EntityFrameworkCore;
using Tiny.Data.Models;

namespace Tiny.Data.Contexts {
    public class TinyDbContext : DbContext {

        public TinyDbContext(DbContextOptions<TinyDbContext> options): base(options) { }

        public virtual DbSet<TinyUrl> TinyUrls { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) =>
            modelBuilder.Entity<TinyUrl>()
                .HasIndex(b => b.LongUrl)
                .HasDatabaseName($"INDEX_{nameof(TinyUrl.LongUrl)}");
    }
}
