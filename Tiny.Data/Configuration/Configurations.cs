﻿
namespace Tiny.Data.Configuration {
    public class RavenConfig {
        public string DefaultDatabase { get; set; }
        public string[] Urls { get; set; }
    }
}
