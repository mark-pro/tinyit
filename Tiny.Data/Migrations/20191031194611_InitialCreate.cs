﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Tiny.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TinyUrls",
                columns: table => new
                {
                    ID = table.Column<string>(maxLength: 7, nullable: false),
                    ShortUrl = table.Column<string>(nullable: true),
                    LongUrl = table.Column<string>(maxLength: 2048, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ExpireAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TinyUrls", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "INDEX_LongUrl",
                table: "TinyUrls",
                column: "LongUrl");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TinyUrls");
        }
    }
}
