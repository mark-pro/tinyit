﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tiny.Data.Models {
    public class TinyUrl {
        [
            Key, 
            StringLength(7, MinimumLength = 7),
            DatabaseGenerated(DatabaseGeneratedOption.None)
        ]
        public string ID { get; set; }
        public string ShortUrl { get; set; }
        [MaxLength(2048)]
        public string LongUrl { get; set; }
        public DateTime CreatedAt { get; set; } = 
            DateTime.Now;
        public DateTime ExpireAt { get; set; }
        public DateTime LastAccessTime { get; private set; } =
            DateTime.Now;
        public TinyUrl UpdateLastAccessTime() {
            this.LastAccessTime = DateTime.Now;
            return this;
        }
    }
}
