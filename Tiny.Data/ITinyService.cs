﻿using System.Threading.Tasks;
using Tiny.Data.Models;

namespace Tiny.Data {
    public interface ITinyService {
        bool TryGetLongUrl(string id , out string longUrl);
        Task<TinyUrl> InsertTinyUrlAsync(TinyUrl tinyUrl);
        Task<bool> TinyUrlExists(string id);
    }
}
