﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Tiny.Data.Configuration;
using Tiny.Data.Contexts;

namespace Tiny.Data.Extensions {
    public static class DependencyExtensions {
        #region ef

        public static IServiceCollection AddTinySql (
            this IServiceCollection services ,
            string connectionString,
            bool useInMemoryDatabase = false
        ) =>
            services.AddDbContext<TinyDbContext>(options => {
                if (useInMemoryDatabase) {
                    options.UseInMemoryDatabase("tiny");
                    options.EnableDetailedErrors();
                    options.EnableSensitiveDataLogging();
                }
                else options.UseSqlServer(connectionString);
            }).AddScoped<TinySqlService>();

        #endregion

        #region raven

        public static IConfigurationBuilder AddTinyDbConfiguration(this IConfigurationBuilder config) =>
            config.AddJsonFile("dbsettings.json" , optional: true , reloadOnChange: true);

        public static IServiceCollection AddRavenContext(this IServiceCollection services , IConfiguration config) =>
            services.Configure<RavenConfig>(config)
                .AddRavenContext()
                .AddScoped<TinyRavenService>();

        public static IServiceCollection AddRavenContext(
            this IServiceCollection services , Action<RavenConfig> configureOptions
        ) => services.Configure(configureOptions)
                .AddRavenContext();

        public static IServiceCollection AddRavenContext(this IServiceCollection services) =>
            services.AddScoped<RavenDbContext>();

        #endregion
    }
}
