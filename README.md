# Tiny IT

## Development Dependencies

- [nodejs 17.*](https://nodejs.org/en/)
- [yarn 1.22.*](https://yarnpkg.com/lang/en/)
- [.NET 6.x](https://dotnet.microsoft.com/download)
- [powershell 5.*](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6&viewFallbackFrom=powershell-5) _only for iana tld download_

_It is recommended you use Visual Studio 2019 and that __Node.js development__ and __ASP.NET and web development__ 
are selected in the __Visual Studio Installer__._

## Configuration

Configurations are done in both the `appsettings.json` and `appsettings.development.json` files.
`Tiny.Worker` and `Tiny.Web` both have _appsettings_ files.

Setting the `UseInMemoryDatabase` to true will ignore the provided connection string for `TinyTbContext`.

### SQL Connection

The application will use an In Memory SQL connection if specified. It is not recommended to use In Memory databases in either stage
or production environments. 

The connection string should be placed in `appsettings.json` as

```json 
{
	"ConnectionStrings": {
		"TinyDbContext": "...sql server connection string"
	},
	"UseInMemoryDatabase": false
}
```

The connection string has been ommited from the repository as [user secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-3.0&tabs=windows) 
are more appropriate when storing connection strings during active development.

Using the In-Memory database option allows for the rapid development of an initial application and its data
structures rather than relying on EF Migrations for small changes usually encoundered by the initial development
of an application. When an application is released into production then EF migrations may be used for database
model changes.

When using `docker-compose` for development it is recommended to turn off the use of in memory databases as an MSSQL Docker container has been provided
in the docker-compose development environment.

_Visit [sql server connection strings](https://www.connectionstrings.com/sql-server/) for more infromation._

### RavenDb Connection

TinyIT is also able to make use of [RavenDb](https://ravendb.net). Connection configuration can be provided in the
`appsettings.json` or `appsettings.development.json` files. For more information see 
[creating a document store](https://ravendb.net/docs/article-page/4.2/Csharp/client-api/creating-document-store)

```json
{
  "RavenDb": {
    "DefaultDatabase": null,
    "Urls": [ ]
  },
  "UseRavenDb": true
}
```
The use of `RavenDb` can be turned on or off with the setting `UseRavenDb`.

__Note__ Both `RavenDb` and `mssql` will be used when RavenDb as support is experimental.

## Configuring For Standalone Development

Both `Tiny.Worker` and `Tiny.Web` can be run individually using dotnet run.

When running the projects as standalone development apps `appsettings.development.json` should be set as follows:
```json
{
	"ConnectionStrings": {
		"TinyDbContext": "...sql server connection string"
	},
	"UseInMemoryDatabase": true
}
```

## Building & Running In Docker

The option to build and run the solution in docker is available for a consistent development environment.
Running in docker can be accomplished by selecting the _docker-compose_ project as the start-up project.
it is not recommented to run TinyIT via docker for production, but Docker containerization is
useful when testing an application or to avoid the phrase "it works in my environment".

If docker is your prefered method of running then it is recommened that 
[Docker for Windows](https://docs.docker.com/docker-for-windows/) is installed and
that Hyper-V is enabled. Hyper-V will not run along side VMWare without special considerations (see below).

- Install Hyper-V
- `bcdedit /copy "{current}" /d "Hyper-V"`
- `bcdedit /set "{current}" hypervisorlaunchtype off`

After configurations have been done restart your machine and select the proper boot options.

#### Configuring For Development In Docker

When running in docker compose the development settings should look as follows:


```json
{
	"ConnectionStrings": {
		"TinyDbContext": "...sql server connection string"
	},
	"UseInMemoryDatabase": false
}
```

Setting the in memory database allows for the continuous change of the database and allows each app to operate indepentently.
It should be set to `false` when using docker as the apps will work against the same database.

__Note__ the current `docker-compose.yml` file requires that there be a network called `data-network` which assumes that an
[mssql container](https://hub.docker.com/_/microsoft-mssql-server) is running.