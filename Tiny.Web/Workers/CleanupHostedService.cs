﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tiny.Data.Contexts;
using Tiny.Data.Models;

namespace Tiny.Web.Workers {
    public class CleanupHostedService : IHostedService, IDisposable {

        private readonly ILogger<CleanupHostedService> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private Timer _timer;

        public CleanupHostedService(IServiceScopeFactory scopeFactory , ILogger<CleanupHostedService> logger) =>
            (_scopeFactory, _logger) = (scopeFactory, logger);

        public Task StartAsync(CancellationToken cancellationToken) {
            //. clean up old entries older than 6 months every day
            _timer = new Timer(CleanUp ,
                null , TimeSpan.Zero , TimeSpan.FromDays(1)
            );

            return Task.CompletedTask;
        }

        private void CleanUp(object state) {
            _logger.LogInformation("started cleanup");
            using var scope = _scopeFactory.CreateScope();
            using var dbContext = scope.ServiceProvider.GetRequiredService<TinyDbContext>();
            var entriesToremove =
                dbContext.Set<TinyUrl>()
                    .Where(i => i.ExpireAt < DateTime.Now.AddMonths(-6));
            dbContext.Set<TinyUrl>().RemoveRange(entriesToremove);
            dbContext.SaveChanges();
        }

        public Task StopAsync(CancellationToken cancellationToken) {
            _timer?.Change(Timeout.Infinite , 0);

            return Task.CompletedTask;
        }
        public void Dispose() {
            _timer?.Dispose();
        }
    }
}
