using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore;
using System;
using Tiny.Data.Extensions;

namespace Tiny.Web {
    public class Program {

        public static bool IS_DEVELOPMENT =>
            Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

        public static void Main(string[] args) {
            CreateWebHostBuilder(args)
                .ConfigureAppConfiguration((hostingContext , config) =>
                    config.AddJsonFile("iana-tlds.json" , optional: false , reloadOnChange: true)
                        .AddTinyDbConfiguration()
                ).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseIIS()
                .UseStartup<Startup>();
                
    }
}
