﻿using System.Collections.Generic;

namespace Tiny.Web.Configuration {
    public class IanaTopLevelDomains {
        public IEnumerable<string> TopLevelDomains { get; set; }
    }
}
