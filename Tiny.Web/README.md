# Tiny.Web

The API and user facing web application for the TinyIT project.

#### Considerations

A list of Top Level Domains is downloaded from the internet. The list is used for quick verification
of appropriate top level domains as approved by [IANA](https://www.iana.org). The script that handles the
file download is `./download_tlds.ps1`, before running the project ensure that the file 
`iana-tlds.json` is present, if it is not run `download_tlds.ps1` using this command from the root 
of the project `powershell -File download_tlds.ps1.` 
Downloading every time on pre-build is _optional_, but recommended as an up-to-date list ensures that the
application functions as intended.

## Intended Actions

When a URL is submitted several checks are performed both local and via the remote API to ensure that a URL is correct
and contains proper URL characters as well as that the string provided is URL safe. Domains are also verified against an
[IANA](https://www.iana.org) maintained list of top level domains which will help with both security and will also
prevent a malicious URL from being generated. This, of course, means that the domain must be public or, if internal,
that the internal domain has a valid TLD.

#### Accepted Domains

- example.com
- sub.examble.com
- external.asd1230-123.asd_internal.asd.gm-_ail.com __worst case__

#### Forbidden Domains

- -example.com
- example.local
- example
- sub.-example.com

#### User Interface

The user interface of the application is simple; an input element and a display element. The display element
will display a list of 50 domains FIFO. _Only 50 domains will be displayed at a time._
The users list of 50 urls will persist the next time the user visits the site unless browser
data is cleared.

#### API

The api has two endpoints that can be hit for producing a short url and redirection of a short url.

Visit the [API Documentation](https://documenter.getpostman.com/view/355830/SW11WJGZ) for mor informaion.

## Configuration

#### Conditions

- __UseRavenDb `bool`__ uses raven db as a supplimentary database, will not replace sql server.
  Used to provide pub/sub to other applciations if desired or to allow raven specic features
- __UseInMemoryDb `bool`__ specify whether the sql server to use should be in memory. Great oprion
  for development or stand alone container

#### ConnectionStrings
- __TinyDbContext `string`__ the connections string to use for the sql server. _Needs to be specified if
  `UseInMemoryDb` is set to false

#### RavenDb
- __DefaultDatabase `string`__ the database to use for Raven. Will only be use if `UseRavenDb` is set to true.
  _If the database does not exist it will be created._
- __Urls `string[]`__ specify the RavenDb server urls with ports to use for communicating with RavenDb.
  Will only be use if `UseRavenDb` is set to true.

#### Sample Config `appsettings.json`
```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Debug",
      "System": "Information",
      "Microsoft": "Information"
    }
  },
  "ConnectionStrings": {
    "TinyDbContext": null
  },
  "RavenDb": {
    "DefaultDatabase": "TinyIT",
    "Urls": [ "http://localhost:8080" ]
  },
  "UseInMemoryDatabase": true,
  "UseRavenDb": true
}
```