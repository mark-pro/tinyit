$r = (iwr "https://data.iana.org/TLD/tlds-alpha-by-domain.txt").Content.Split([Environment]::NewLine) | 
	?{ !$_.Contains("#") -and $_ -ne [String]::Empty }

@{ TopLevelDomains = $r } | ConvertTo-Json -Compress | Set-Content "iana-tlds.json"