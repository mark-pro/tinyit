using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Tiny.Web.Configuration;
using Tiny.Data.Contexts;
using Tiny.Data.Extensions;
using Microsoft.EntityFrameworkCore;
using System;

namespace Tiny.Web {
    public class Startup {

        public Startup(IConfiguration configuration) =>
            Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {

            services.Configure<IanaTopLevelDomains>(Configuration)
                .AddControllersWithViews();

            (bool useRaven, bool useSql) = (
                Configuration.GetValue<bool>("UseRavenDb"),
                Configuration.GetValue<bool>("UseSql")
            );
            if (!useRaven && !useSql)
                throw new Exception("must use either raven, sql or both");

            if (useRaven)
                services.AddRavenContext(Configuration.GetSection("RavenDb"));

            if(useSql)
                services.AddTinySql(
                    connectionString: Configuration.GetConnectionString(nameof(TinyDbContext)) ,
                    useInMemoryDatabase: Configuration.GetValue<bool>("UseInMemoryDatabase")
                );
            
            //. In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
                configuration.RootPath = "ClientApp/build");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app , IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "default" ,
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa => {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment()) {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });

            var sp = app.ApplicationServices
                    .CreateScope()
                    .ServiceProvider;

            if (Configuration.GetValue<bool>("UseRavenDb"))
                sp.GetRequiredService<RavenDbContext>().EnsureCreated();

            if(Configuration.GetValue<bool>("UseSql"))
                if (env.IsDevelopment() || Configuration.GetValue<bool>("UseInMemoryDatabase"))
                    sp.GetRequiredService<TinyDbContext>()
                        .Database.EnsureCreated();
                else if (env.IsStaging() || env.IsProduction())
                    sp.GetRequiredService<TinyDbContext>()
                        .Database.Migrate();
        }
    }
}
