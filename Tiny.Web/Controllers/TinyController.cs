﻿using System;
using Microsoft.AspNetCore.Mvc;
using Base62;
using System.Security.Cryptography;
using System.Linq;
using Tiny.Data.Models;
using System.Threading.Tasks;
using System.Net;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Tiny.Web.Configuration;
using System.Text.RegularExpressions;
using Tiny.Data;

namespace Tiny.Web.Controllers
{
    [Route("")]
    [ApiController]
    public class TinyController : ControllerBase, IDisposable {

        private readonly TinySqlService _sqlService;
        private readonly TinyRavenService _ravenService;

        private readonly IanaTopLevelDomains _tlds;
        private const string domainRegex = 
            @"^([a-zA-Z0-9][a-zA-Z0-9-_]*\.)+[a-zA-Z0-9]*[a-zA-Z0-9-_]*[[a-zA-Z0-9]{2,}$";

        public TinyController(
            IOptionsMonitor<IanaTopLevelDomains> tlds,
            TinySqlService sqlService = null, 
            TinyRavenService ravenService = null) =>
                (_tlds, _sqlService, _ravenService) = 
                    (tlds.CurrentValue, sqlService, ravenService);
        
        private string RequestBase =>
            $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}/";

        /// <summary>
        /// Gets or creates a shortened url from a provided uri.
        /// </summary>
        /// <param name="uri">The long uri to shorten or retrieve</param>
        /// <returns>Action result of success or error</returns>
        [HttpPut("tiny/it")]
        public async Task<IActionResult> GetShortenedUrl([FromBody] string uri) {
            
            //. validate if uri is acceptable for insertion
            if (uri.Contains(HttpContext.Request.Host.Host))
                return StatusCode((int) HttpStatusCode.Forbidden);
            
            if (!Uri.TryCreate(uri , UriKind.Absolute , out var u) || !IsValidDomain(u.Host))
                return StatusCode((int) HttpStatusCode.Forbidden);

            TinyUrl tinyUrl = null;
            //. if it does not already exist
            do {
                //. 5 bytes will give us 62^7 or 3.52 e^12 available options 
                //. and populate with cryptografic random numbers
                var r = GenerateBase62(new byte[5]);

                //. do a cheap read to see if id already exists
                if (await TinyUrlAlreadyExists(r))
                    continue;

                //. insert tiny url
                tinyUrl = CreateTinyUrl(r , uri);

                //. validate that we made a correct model
                if (!IsValidTinyUrl(tinyUrl))
                    return StatusCode((int) HttpStatusCode.InternalServerError);

                tinyUrl = await InsertTinyUrl(tinyUrl);
            } while (tinyUrl == null);

            return Ok(tinyUrl);
        }

        /// <summary>
        /// Redirects to a url given an id.
        /// If no id is provided then redirect to home
        /// </summary>
        /// <param name="id">The id of the short url</param>
        /// <returns>Redirect action to either external or home</returns>
        [HttpGet("{id}")]
        public IActionResult Go([FromRoute] string id) =>
            Redirect(GetLongUrl(id) ?? RequestBase);

        private bool IsValidDomain(string host) =>
            Regex.IsMatch(host , domainRegex) &&
            _tlds.TopLevelDomains.Contains(host.Split('.').Last().ToUpper());

        private string GenerateBase62(byte[] bytes) {
            RandomNumberGenerator.Create().GetNonZeroBytes(bytes);
            return bytes.ToBase62();
        }

        private async Task<bool> TinyUrlAlreadyExists(string id) =>
            (bool?) await _ravenService?.TinyUrlExists(id) ?? 
                (bool?) await _sqlService?.TinyUrlExists(id) ?? false;

        private async Task<TinyUrl> InsertTinyUrl(TinyUrl tinyUrl) =>
            await _ravenService?.InsertTinyUrlAsync(tinyUrl) ??
                await _sqlService?.InsertTinyUrlAsync(tinyUrl);

        private string GetLongUrl(string id) {
            string longUrl = null;
            _ = (_sqlService?.TryGetLongUrl(id , out longUrl) ?? false) |
                (_ravenService?.TryGetLongUrl(id , out longUrl) ?? false);
            return longUrl;
        }

        private bool IsValidTinyUrl(TinyUrl tinyUrl) {
            var vContext = new ValidationContext(tinyUrl , null);
            return Validator.TryValidateObject(tinyUrl , vContext , new List<ValidationResult>());
        }

        private TinyUrl CreateTinyUrl(string id, string uri) =>
            new TinyUrl {
                ID = id ,
                LongUrl = uri ,
                ShortUrl = RequestBase + id ,
                CreatedAt = DateTime.Now ,
                ExpireAt = DateTime.Now.AddMonths(3)
            }.UpdateLastAccessTime();

        public void Dispose() { 
            _sqlService?.Dispose();
            _ravenService?.Dispose();
        }
    }
}