﻿import { createMuiTheme } from '@material-ui/core/styles'
import { deepOrange, blue } from '@material-ui/core/colors'

// @ts-ignore
const theme = createMuiTheme({
    palette: {
        background: {
            default: '#282c34'
        },
        primary: {
            main: blue[700],
            dark: blue[800]
        },
        secondary: {
            main: deepOrange[800],
            dark: deepOrange[900]
        },
        type: 'dark'
    }
})

export default theme