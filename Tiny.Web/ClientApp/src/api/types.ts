﻿
export interface IApiResponse {
    id: string
    shortUrl: string
    longUrl: string
    createdAt: Date
    expireAt: Date
}