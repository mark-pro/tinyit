﻿import {
    makeStyles,
    createStyles,
    Theme
} from '@material-ui/core/styles'

const appBgColor = '#282c34'

export const AppStyles = makeStyles((theme: Theme) =>
    createStyles({
        app: {
            textAlign: 'center',
            backgroundColor: appBgColor,
            minHeight: '100vh'
        },
        appHeader: {
            minHeight: '30vh',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            color: theme.palette.getContrastText(appBgColor)
        },
        appContainer: {
            padding: '1.5em'
        },
        notistackText: {
            color: 'white'
        }
    })
)