﻿import React from 'react'
import TinyInput from '../TinyInput'
import {
    Container,
    Typography,
    IconButton
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import { AppStyles } from './Styles'
import { ThemeProvider } from '@material-ui/styles'
import { SnackbarProvider } from 'notistack'
import theme from '../../Theme'
import TinyDisplay from '../TinyDisplay'
import { IApiResponse } from '../../api/types'

const App: React.FC = () => {
    const classes = AppStyles()

    const notistackRef = React.createRef()

    const handleSnackDismiss = (key: string | number) => () => {
        (notistackRef.current as any).closeSnackbar(key)
    }

    const [tinyUrls, setTinyUrls] = React.useState <IApiResponse[]>([])

    React.useEffect(() => {
        setTinyUrls(JSON.parse(window.localStorage.getItem('urls') || '[]') as IApiResponse[])
    }, [])

    return (
        <ThemeProvider theme={theme}>
            <SnackbarProvider
                ref={notistackRef}
                action={(key: string | number) => (
                    <IconButton onClick={handleSnackDismiss(key)}>
                        <Close />
                    </IconButton>
                )}
                classes={{
                    variantSuccess: classes.notistackText,
                    variantError: classes.notistackText,
                    variantWarning: classes.notistackText,
                    variantInfo: classes.notistackText
                }}
                maxSnack={3}
            >
                <div className={classes.app}>
                    <Container className={classes.appContainer}>
                        <Typography
                            className={classes.appHeader}
                            variant='h2'
                        >
                            Tiny.it
                        </Typography>
                        <TinyInput
                            onAdd={tinyUrl => {

                                //. display a unique list of 50 tiny urls
                                //. FILO
                                const updatedDisplay = [
                                    tinyUrl,
                                    ...tinyUrls
                                        .filter(v => v.id !== tinyUrl.id).slice(0, 50)
                                ]
                                setTinyUrls(updatedDisplay)
                                window.localStorage.setItem('urls', JSON.stringify(updatedDisplay))
                            }}
                        />
                        {
                            //. Do not display TinyDisplay where no content
                            tinyUrls.length > 0 ?
                                <TinyDisplay
                                    tinyUrls={tinyUrls}
                            /> : null
                        }
                    </Container>
                </div>
            </SnackbarProvider>
        </ThemeProvider>
    );
}

export default App;
