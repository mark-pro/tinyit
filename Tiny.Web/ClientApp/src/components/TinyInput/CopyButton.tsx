﻿import React from 'react'
import {
    Button, Theme
} from '@material-ui/core'
import { FileCopyOutlined, DoneAll } from '@material-ui/icons'
import { IButtonProps } from './types'
import { withStyles } from '@material-ui/styles'
import { CopybuttonStyle } from './Styles'

const CopyButtonBase = withStyles((theme: Theme) =>
    CopybuttonStyle(theme)    
)(Button)

interface ICopyButtonProps {
    copied: boolean
}

const CopyButton: React.FC<IButtonProps & ICopyButtonProps> = props =>
    <CopyButtonBase
        variant='contained'
        onClick={props.onClick}
    >
        {
            props.copied ?
                <DoneAll />
            : <FileCopyOutlined />
        }
    </CopyButtonBase>

export default CopyButton