﻿import React from 'react'
import { SendButtonStlye } from './Styles'
import { withStyles } from '@material-ui/styles'
import { Theme, Button } from '@material-ui/core'
import { Send } from '@material-ui/icons'
import { IButtonProps } from './types'

const SendButtonBase = withStyles((theme: Theme) =>
    SendButtonStlye(theme)
)(Button)

const SendButton: React.FC<IButtonProps> = props => {
    return (
        <SendButtonBase
            color='primary'
            variant='contained'
            onClick={props.onClick}
        >
            <Send />
        </SendButtonBase>
    )
}

export default SendButton