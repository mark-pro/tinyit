﻿import React from 'react'
import { IApiResponse } from '../../api/types';

export interface IButtonProps {
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void
}

export interface ITinyInputProps {
    onAdd: (tinyUrl: IApiResponse) => void
}