﻿import React from 'react'
import {
    Grid,    Paper,
    LinearProgress
} from '@material-ui/core'
import Styles from './Styles'
import SendButton from './SendButton'
import TinyInputTextField from './TinyInputTextField'
import { IApiResponse } from '../../api/types'
import CopyButton from './CopyButton'
import { withSnackbar, WithSnackbarProps } from 'notistack'
import { ITinyInputProps } from './types'

const TinyInput: React.FC<ITinyInputProps & WithSnackbarProps> = props => {

    const [url, setUrl] = React.useState<string>('')
    const [loading, setLoading] = React.useState<boolean>(false)
    const [shortUrl, setShortUrl] = React.useState<string>()
    const [displayShortUrl, setDisplayShortUrl] = React.useState<boolean>(false)
    const [copied, setCopied] = React.useState<boolean>(false)
    const textFieldRef = React.createRef<HTMLInputElement>()
     
    const classes = Styles()

    const isValidUrl = (url: string): boolean => {
        try {
            new URL(url)
            return true
        }
        catch {
            return false
        }
    }

    const displayErrorSnack = () => {
        props.enqueueSnackbar('could not create tiny url ensure the url is correct', {
            variant: 'error'
        })
    }

    const handleSendButtonClick = () => {
        setLoading(true)
        const cUrl = url && !(url.startsWith('http://') || url.startsWith('https://')) ?
            `http://${url}` : url || ''

        if (isValidUrl(cUrl)) {
            fetch('/tiny/it', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(cUrl)
            })
                .then(response => {
                    if (response.status !== 200)
                        throw new Error('did not receive a 200 statuscode from the server')

                    return response.json()
                })
                .then((json: IApiResponse) => {
                    console.log(json)
                    setShortUrl(json.shortUrl)
                    props.onAdd(json)
                    setDisplayShortUrl(true)
                })
                .catch(() => {
                    setLoading(false)
                    displayErrorSnack()
                })
                .finally(() => {
                    setLoading(false)
                })
        }
        else {
            displayErrorSnack()
            setLoading(false)
        }
    }

    const handleCopyButtonClick = () => {
        navigator.clipboard.writeText(shortUrl || '')
            .then(() => {
                setCopied(true)
                props.enqueueSnackbar('copied to clipboard', {
                    variant: 'success'
                })
            })
            .catch(() => {
                props.enqueueSnackbar('could not copy, try ctrl + c', { variant: 'warning' })
            })
        
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) =>
        event.key === 'Enter' && handleSendButtonClick()

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUrl(event.target.value)
        displayShortUrl && setDisplayShortUrl(false)
        copied && setCopied(false)
    }

    return (
        <div>
            <Paper className={classes.paper}>
                <Grid container
                >
                    <Grid item xs={10} sm={10} md={11}>
                        <TinyInputTextField
                            inputRef={textFieldRef}
                            onChange={handleInputChange}
                            value={displayShortUrl ? shortUrl : url}
                            onKeyPress={handleKeyPress}
                            autoFocus
                            fullWidth
                            placeholder='http://my-super-long-url/'
                        />
                    </Grid>
                    <Grid item xs={2} sm={2} md={1}>
                        {
                            displayShortUrl ?
                                <CopyButton
                                    copied={copied}
                                    onClick={handleCopyButtonClick}
                                /> :
                                <SendButton onClick={handleSendButtonClick} />
                        }
                    </Grid>
                </Grid>
                {
                    loading &&
                    <LinearProgress color='secondary' className={classes.progress} />
                }
            </Paper>
        </div>
    )
}


export default withSnackbar(TinyInput)