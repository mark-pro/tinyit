﻿import { TinyInputTextFieldStyle } from './Styles'
import { InputBase, withStyles, Theme } from '@material-ui/core'

const TinyInputTextField = withStyles((theme: Theme) =>
    TinyInputTextFieldStyle(theme)
)(InputBase)

export default TinyInputTextField