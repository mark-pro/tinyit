﻿import {
    makeStyles,
    createStyles,
    Theme
} from '@material-ui/core/styles'
import { CSSProperties } from '@material-ui/styles'
import { green } from '@material-ui/core/colors'

export const TinyInputTextFieldStyle = (theme: Theme) =>
    createStyles({
        root: {
            'label + &': {
                marginTop: theme.spacing(3),
            }
        },
        input: {
            borderRadius: 4,
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
            fontSize: 24,
            padding: '.25em .25em',
        }
    })

const commonButtonStyle: CSSProperties = {
    padding: '10px 12px',
    borderRadius: 4,
    height: '100%',
    width: '100%'
}

export const SendButtonStlye = (theme: Theme) =>
    createStyles({
        root: { ...commonButtonStyle }
    })

export const CopybuttonStyle = (theme: Theme) =>
    createStyles({
        root: {
            ...commonButtonStyle,
            backgroundColor: green[600],
            color: theme.palette.getContrastText(green[600]),
            '&:hover': {
                backgroundColor: green[700],
                color: theme.palette.getContrastText(green[700])
            }
        }
    })

const Styles = makeStyles((theme: Theme) => 
    createStyles({
        paper: {
            padding: theme.spacing(1),
            paddingRight: theme.spacing(1.3)
        },
        progress: {
            borderRadius: '4px'
        }
    })
)

export default Styles