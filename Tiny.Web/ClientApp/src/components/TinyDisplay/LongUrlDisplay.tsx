﻿import React from 'react'
import { ILongUrlDisplayProps } from './types'
import { Hidden } from '@material-ui/core'

const LongUrlDisplay: React.FC<ILongUrlDisplayProps> = (props: ILongUrlDisplayProps) => {

    const getElipses = (take: number) =>
        props.longUrl.length > take ? '...' : ''

    const displayText = {
        short: props.longUrl.slice(0, 30) + getElipses(30),
        medium: props.longUrl.slice(0, 55) + getElipses(55),
        large: props.longUrl.slice(0, 90) + getElipses(90),
        extraLarge: props.longUrl.slice(0, 120) + getElipses(120)
    }

    return (
        <React.Fragment>
            <Hidden smUp>
                {displayText.short}
            </Hidden>
            <Hidden mdUp xsDown>
                {displayText.medium}
            </Hidden>
            <Hidden smDown lgUp>
                {displayText.large}
            </Hidden>
            <Hidden mdDown>
                {displayText.extraLarge}
            </Hidden>
        </React.Fragment>
    )
}

export default LongUrlDisplay