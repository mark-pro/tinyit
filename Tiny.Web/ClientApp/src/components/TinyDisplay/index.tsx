﻿import React from 'react'
import { ITinyDisplayProps } from './types'
import {
    Paper,
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,    Divider,
    Button,
    ButtonGroup
} from '@material-ui/core'
import { WithSnackbarProps, withSnackbar } from 'notistack'
import Style from './Styles'
import LongUrlDisplay from './LongUrlDisplay'
import { OpenInNew, FileCopyOutlined } from '@material-ui/icons'

const TinyDisplay: React.FC<ITinyDisplayProps & WithSnackbarProps> = props => {

    const classes = Style()

    const handleCopyClick = (shortUrl: string) => {
        navigator.clipboard.writeText(shortUrl)
        props.enqueueSnackbar('copied to clipboard!', {
            variant: 'success'
        })
    }

    return (
        <Paper className={classes.paper}>
            <List className={classes.list}>
                {
                    props.tinyUrls.map(d => (
                        <ListItem key={d.id} button onClick={() => handleCopyClick(d.shortUrl)}>
                            <ListItemText
                                primary={d.id}
                                secondary={
                                    <LongUrlDisplay longUrl={d.longUrl} />
                                }
                            >
                            </ListItemText>
                            <ListItemSecondaryAction>
                                <ButtonGroup variant='outlined'>
                                    <Button
                                        onClick={() => window.open(d.longUrl)}
                                    >
                                        <OpenInNew />
                                    </Button>
                                    <Button
                                        onClick={() => handleCopyClick(d.shortUrl)}
                                    >
                                        <FileCopyOutlined />
                                    </Button>
                                </ButtonGroup>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )).reduce((prev, curr) => (
                        <React.Fragment>
                            {prev}
                            <Divider />
                            {curr}
                        </React.Fragment>
                    ))
                }
            </List>
        </Paper>    
    )
}

export default withSnackbar(TinyDisplay)