﻿import { IApiResponse } from "../../api/types";

export interface ITinyDisplayProps {
    tinyUrls: IApiResponse[]
}

export interface ILongUrlDisplayProps {
    longUrl: string
}