﻿import {
    createStyles,
    makeStyles,
    Theme
} from '@material-ui/core/styles'

const TinyDisplayStyle = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            marginTop: theme.spacing(2)
        },
        list: {
            minHeight: '30vh',
            maxHeight: '50vh',
            overflow: 'auto'
        }
    })
)

export default TinyDisplayStyle