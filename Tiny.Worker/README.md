﻿# Tiny.Worker

The tiny worker is a services that will remove entries on a periodic basis 
that are older than an `n` specified number of months.

## Configuration

```json
{
	//... other configurations such as logging and connection strings
	"Worker": {
		//. Interval to check for expired exams in Days
		"RunInterval": 1
	}
}
```

## Installation

```
dotnet publish -o c:\code\workerpub
sc create workertest binPath=c:\code\workerpub\WorkerTest.exe

```