using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Tiny.Data.Contexts;
using Tiny.Data.Extensions;

namespace Tiny.Worker {

    public class Program {

        public static void Main(string[] args) =>
            CreateHostBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) => 
                    config
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddJsonFile(path: $"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", 
                            optional: true, 
                            reloadOnChange: true
                    )).Build().Run();

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext , services) => {
                    if (hostContext.Configuration.GetValue<bool>("UseRavenDb"))
                        services.AddRavenContext(hostContext.Configuration.GetSection("RavenDb"));

                    services
                        .Configure<WorkerConfiguration>(hostContext.Configuration.GetSection("Worker"))
                        .AddTinySql(
                            connectionString: hostContext.Configuration.GetConnectionString(nameof(TinyDbContext)) ,
                            useInMemoryDatabase: hostContext.Configuration.GetValue<bool>("UseInMemoryDatabase")
                        ).AddHostedService<CleanupWorker>();
                });
    }
}
