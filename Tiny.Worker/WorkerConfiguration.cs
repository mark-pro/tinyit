﻿
namespace Tiny.Worker {
    public class WorkerConfiguration {

        /// <summary>
        /// Interval to check for expired exams
        /// in Days
        /// </summary>
        public int RunInterval { get; set; }

        /// <summary>
        /// Remove exams older than n months
        /// </summary>
        public int RemoveOlderThan { get; set; }

        /// <summary>
        /// Enables sliding expiration based on last access time
        /// </summary>
        public bool EnableSlidingExpiration { get; set; }
    }
}
