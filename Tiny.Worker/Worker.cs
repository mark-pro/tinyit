using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Raven.Client.Documents.Session;
using Tiny.Data.Contexts;
using Tiny.Data.Models;

namespace Tiny.Worker {
    public class CleanupWorker : IHostedService, IDisposable {

        private readonly ILogger<CleanupWorker> _logger;
        private readonly IServiceScope _serviceScope;
        private readonly IDocumentSession _ravenSession;
        private readonly TinyDbContext _tinyContext;
        private Timer _timer;
        private readonly WorkerConfiguration _workerConfig;

        public CleanupWorker(
            IServiceScopeFactory scopeFactory , 
            ILogger<CleanupWorker> logger, 
            IOptionsMonitor<WorkerConfiguration> workerConfig) {
            
            (_serviceScope, _logger, _workerConfig) = (
                scopeFactory.CreateScope(), logger, workerConfig.CurrentValue
            );
            var sp = _serviceScope.ServiceProvider;
            _tinyContext = sp.GetRequiredService<TinyDbContext>();
            _ravenSession = sp.GetService<RavenDbContext>()?.Store.OpenSession();
        }

        public Task StartAsync(CancellationToken cancellationToken) {
            //. clean up old entries older than 6 months every day
            _timer = new Timer(CleanUp ,
                null , TimeSpan.Zero , TimeSpan.FromDays(_workerConfig.RunInterval)
            );

            return Task.CompletedTask;
        }

        private async void CleanUp(object state) {
            _logger.LogInformation("started cleanup");

            _tinyContext.Database.EnsureCreated();

            Func<TinyUrl, bool> expirePred = i =>
                (_workerConfig.EnableSlidingExpiration ? i.LastAccessTime : i.ExpireAt) < DateTime.Now;

            var entriesToremove =
                _tinyContext.Set<TinyUrl>()
                    .Where(expirePred);
            _tinyContext.Set<TinyUrl>().RemoveRange(entriesToremove);
            await _tinyContext.SaveChangesAsync();

            var ravenEntriesToRemove = _ravenSession?.Query<TinyUrl>()
                .Where(expirePred);
            
            if(ravenEntriesToRemove != null)
                foreach(var e in ravenEntriesToRemove)
                    _ravenSession?.Delete(e);

            _ravenSession?.Advanced.WaitForReplicationAfterSaveChanges(replicas: 2);

            _ravenSession?.SaveChanges();

        }

        public Task StopAsync(CancellationToken cancellationToken) {
            _timer?.Change(Timeout.Infinite , 0);

            return Task.CompletedTask;
        }
        public void Dispose() {
            _timer?.Dispose();
            _tinyContext.Dispose();
            _ravenSession?.Dispose();
            _serviceScope.Dispose();
        }
    }
}
